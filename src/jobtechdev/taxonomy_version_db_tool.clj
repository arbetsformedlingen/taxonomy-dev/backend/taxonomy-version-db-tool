(ns jobtechdev.taxonomy-version-db-tool
  (:gen-class)
  (:require [wanderung.core :as w]
            [datomic.client.api :as dc]))

(defn datomic-cfg
  [db-name]
  {:wanderung/type :datomic
   :name           db-name
   :server-type    :ion
   :region         "eu-central-1"
   :system         "tax-prod-v4"
   :endpoint       "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port     8182})

(defn nippy-cfg
  [filepath]
  {:wanderung/type :nippy
   :filename       filepath})

(defn migrate-to-local-db
  [prod-write-db nippy-db]
  (w/migrate prod-write-db nippy-db))

(defn migrate-local-db-to-prod-read
  [nippy-db prod-read-db]
  (w/migrate nippy-db prod-read-db))

(comment

  ;(def client (dc/client datomic-prod-write-cfg))
  ;(dc/create-database client {:db-name "jobtech-taxonomy-prod-read-v20"})
  )

